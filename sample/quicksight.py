import boto3
from docs.config import AWS_ACCOUNT_ID

class Quicksight:
    def __init__(self, *args, **kwargs):
        """
        Example parameters:
            - aws_access_key_id
            - aws_secret_access_key
            - region_name
        """
        self.quicksight_client = boto3.client("quicksight",
            *args, 
            **kwargs)


    def get_iterators_with_pagination(self, operation_name, all_iterators=False, **parameters):
        """
        Returns a dict of iterator(s). 
        
        There can be more than one iterators if all_iterators is True.

        The available paginators are:
            QuickSight.Paginator.ListAnalyses
            QuickSight.Paginator.ListDashboardVersions
            QuickSight.Paginator.ListDashboards
            QuickSight.Paginator.ListDataSets
            QuickSight.Paginator.ListDataSources
            QuickSight.Paginator.ListIngestions
            QuickSight.Paginator.ListNamespaces
            QuickSight.Paginator.ListTemplateAliases
            QuickSight.Paginator.ListTemplateVersions
            QuickSight.Paginator.ListTemplates
            QuickSight.Paginator.ListThemeVersions
            QuickSight.Paginator.ListThemes
            QuickSight.Paginator.SearchAnalyses
            QuickSight.Paginator.SearchDashboards

        Example pagination config:
        PaginationConfig = {
        'MaxItems': 123,
        'PageSize': 123,
        'StartingToken': 'string'
        }

        See more on https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/quicksight.html#paginators
        """
        if self.quicksight_client.can_paginate(operation_name) is False:
            raise TypeError("Operation cannot be paginated")
            
        pagination = self.quicksight_client.get_paginator(operation_name)

        if 'AwsAccountId' not in parameters.keys():
            parameters['AwsAccountId'] = AWS_ACCOUNT_ID
        if 'PaginationConfig' not in parameters.keys():
            parameters['PaginationConfig'] = {}

        iterators = []
        has_next_token = True
        iterator = pagination.paginate(**parameters)

        if all_iterators:    
            while has_next_token:
                for page in iterator:                    
                    iterators.append(page)

                    if 'NextToken' in page.keys():
                        parameters['PaginationConfig']['StartingToken'] = page['NextToken']
                        iterator = pagination.paginate(**parameters)
                    else:
                        has_next_token = False
                 
        return iterators