import os

AWS_ACCESS_KEY_ID = os.environ.get("AwsAccessKeyId")
AWS_SECRET_ACCESS_KEY = os.environ.get("AwsSecretAccessKey")
AWS_DEFAULT_REGION = os.environ.get("AwsDefaultRegion")
AWS_ACCOUNT_ID = os.environ.get("AwsAccountId")