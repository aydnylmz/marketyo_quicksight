from sample.quicksight import Quicksight
from docs.config import AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_DEFAULT_REGION
quicksight = Quicksight(aws_access_key_id=AWS_ACCESS_KEY_ID,
                          aws_secret_access_key=AWS_SECRET_ACCESS_KEY,
                          region_name=AWS_DEFAULT_REGION)
analysis_iterators = quicksight.get_iterators_with_pagination(operation_name='list_analyses',
all_iterators=True)                   
